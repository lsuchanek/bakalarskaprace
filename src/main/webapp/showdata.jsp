<%@ page import="java.util.List" %>
<%@ page import="backend.Turn" %>
<%@ page import="backend.User" %>
<%@ page import="backend.Conversation" %>
<%@ page import="xml.ConversationManager" %><%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 19.09.2018
  Time: 12:51
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<%
    Long conId = (Long) session.getAttribute("conId");
    Conversation conversation = xml.ConversationManager.getConversation(conId);
    List<Turn> turns = conversation.getTurns();
    for(Turn turn:turns){
        if(turn.isUserInitialized()) {
            out.println("<p align=\"left\">U: " + turn.getQuestion() + "</p>");
            if(turn.getReply() != null)
                out.println("<p align=\"right\">" + turn.getReply() + " :W</p>");
        }
        else{
            out.println("<p align=\"right\">" + turn.getQuestion() + " :W</p>");
            if(turn.getReply() != null)
                out.println("<p align=\"left\">U: " + turn.getReply() + "</p>");
        }

    }
    if(!conversation.isActive()){
        out.println("<p style=\"color:red;\"><b>");
        out.println("Conversation was closed, please rate conversation!");
        out.println("</b></p>");
    }
%>
</body>
</html>
