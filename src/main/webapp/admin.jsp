<%@ page import="backend.Main" %>
<%@ page import="xml.WizardManager" %>
<%@ page import="java.util.List" %>
<%@ page import="backend.Wizard" %>
<%@ page import="xml.XmlToString" %><%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 07.05.2019
  Time: 15:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
       Admin Page
    </title>
    <style>
        table, th, td {
            border: 2px solid black;
            border-collapse: collapse;
        }

        th, td {
        padding: 15px;
        }
    </style>
</head>

<body>
<h2>${login}</h2>
<form action="./">
    <input type="submit" value="Log out" />
</form>
<a href="changePassword.jsp" >Change password</a>
<form action="DownloadConversations">
    <input type="hidden" name="type" value="conversations">
    <input type="submit" value="Download Conversations">
</form>
<form method="post" action="JoinConversation">
    <h3>Join conversation</h3>
    <%
        out.println("<input type=\"hidden\" name=\"login\" value=\""+ session.getAttribute("login") +"\">");
        if(request.getAttribute("noConversation") != null){
            out.println("<p style=\"color:red;\">");
            out.println("No conversation available! Please try later");
            out.println("</p>");
        }
    %>
    <input type="submit" value="Join Conversation">
</form>

<h2>Tested grammar</h2>
<form action="DownloadGrammar">
    <input type="hidden" name="type" value="grammar">
    <input type="submit" value="Download Grammar">
</form>
<iframe src="grammar.jsp" width="500" height="300">
</iframe>
<br/>

<button onclick="grammarHideShow()">Edit Grammar</button>
<div id="grammar" style="display:none;">
    <form action="ChangeGrammar" method="post">
        <textarea rows = "30" cols = "60" name="changedGrammar">
             <%
                 out.println(XmlToString.grammarToString());
             %>
        </textarea>
        <input type="submit" value="Save Grammar">
    </form>
</div>
<br/>
<form method="get" action="RuleNoCon">
    ID: <input type="text" name="id">
    <input type="submit" value="Choose rule">
</form>

<form method="post" action="ChangeRule">

         <textarea rows = "10" cols = "50" name="changedRule">
             <%
                 out.println(session.getAttribute("rule"));
             %>
         </textarea>
    <%
        out.println("<input type=\"hidden\" name=\"id\" value=\""+ session.getAttribute("ruleID") +"\">");
    %>
    <input type="submit" value="Change rule">

</form>

<button onclick="myFunction()">Show/hide accounts</button>
<div id="accounts" style="display:none;">
    <h2>Accounts</h2>
            <table>
                <tr>
                    <td>Login</td>
                    <td>Role</td>
                    <td>Change Role</td>
                </tr>
                <%
                   List<Wizard> wizards = WizardManager.getAll();
                   for (Wizard wizard:wizards){
                       if(!wizard.getLogin().equals("admin") && !wizard.getLogin().equals(session.getAttribute("login").toString())) {
                           out.println("<tr>");
                           out.println("<td>" + wizard.getLogin() + "</td>");
                           if (wizard.isAdmin())
                               out.println("<td>admin</td>");
                           else
                               out.println("<td>standard</td>");
                           out.println("<td>");
                           out.println("<form method=\"post\" action=\"ChangeRole\">");
                           out.println("<input type=\"hidden\" name=\"account\" value=\"" + wizard.getLogin() + "\">");
                           out.println("<input type=\"submit\" value=\"Change role\"></form></td>");
                           out.println("<td><form method=\"post\" action=\"DeleteAccount\">");
                           out.println("<input type=\"hidden\" name=\"delname\" value=\"" + wizard.getLogin() + "\">");
                           out.println("<input type=\"submit\" value=\"Delete\"></form></td>");
                           out.println("</tr>");
                       }
                   }
                %>
            </table>

    </div>
<script>
    function myFunction() {
        var e = document.getElementById("accounts");
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }
</script>
<script>
    function grammarHideShow() {
        var e = document.getElementById("grammar");
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }
</script>
</body>
</html>
