<%@ page import="managers.ConversationManager" %>
<%@ page import="backend.Conversation" %><%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 20.09.2018
  Time: 14:58
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Closed Conversation</title>
</head>
<body>
<h1>
    Conversation was closed!
</h1>
<h3>
    Please rate conversation
</h3>
<form method="post" action="WizardEnd">
    Evaluation [0-10]: <input type="text" name="evaluation">
    <%
        ConversationManager conversationManager = (ConversationManager) session.getAttribute("conMan");
        Long conId = (Long) session.getAttribute("conId");
        Conversation conversation = conversationManager.getConversation(conId);
        out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
    %>
    <input type="submit" value="Rate conversation"><br><br>

</form>
</body>
</html>
