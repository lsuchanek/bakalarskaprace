<%@ page import="backend.Conversation" %>
<%@ page import="backend.User" %>
<%@ page import="java.io.*"%>
<%@ page import="xml.ConversationManager" %>
<%@ page import="backend.Turn" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 11.09.2018
  Time: 15:35
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
<head>

    <title>Chat</title>
    <style>
        .chat {
            width: 500px;
            padding: 10px;
            border: 2px solid black;
            margin-right: auto;
            margin-left: auto;
        }

        .tooltip {
            position: relative;
            display: inline-block;
        }

        .tooltip .tooltiptext {
            visibility: hidden;
            width: 300px;
            background-color: #555;
            color: #fff;
            text-align: center;
            border-radius: 6px;
            padding: 5px 0;
            position: absolute;
            z-index: 1;
            bottom: 125%;
            left: 50%;
            margin-left: -60px;
            opacity: 0;
            transition: opacity 0.3s;
        }

        .tooltip .tooltiptext::after {
            content: "";
            position: absolute;
            top: 100%;
            left: 50%;
            margin-left: -5px;
            border-width: 5px;
            border-style: solid;
            border-color: #555 transparent transparent transparent;
        }

        .tooltip:hover .tooltiptext {
            visibility: visible;
            opacity: 1;
        }
    </style>
    <script type="text/javascript" src="jquery-3.3.1.js"></script>
    <script type="text/javascript">
        var auto_refresh = setInterval(
            function ()
            {
                $('#load_me').load('showdata.jsp').fadeIn("slow");
            }, 1000); // autorefresh the content of the div after

    </script>

</head>
<body>

<center>
<h1>
    Chat
</h1>
    <iframe src="grammar.jsp" width="500" height="300">

    </iframe>
    <br>
   <%-- <div class="chat">
        <%
            Long conId = (Long) session.getAttribute("conId");
            Conversation conversation = ConversationManager.getConversation(conId);
            List<Turn> turns = conversation.getTurns();
        %>
    </div>--%>

    <div id="load_me" class="chat">
        <%@ include file="showdata.jsp" %>
    </div>
    <br>
    <form method="post" action="WizardReply">
        <input type="text" name="answer" size="45">
        <%
            out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
        %>
        <input type="submit" value="Send"><br><br>

    </form>
    </br>
    <form method="post" action="WizardEnd">
        Evaluation [0-10]: <input type="text" name="evaluation">
        <%
            out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
        %>
        <input type="submit" value="End conversation"><br><br>

    </form>
    <form method="get" action="RuleID">
        ID: <input type="text" name="id">
        <%
            out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
        %>
        <input type="submit" value="Choose rule">
    </form>

     <form method="post" action="AddRule">

         <textarea rows = "10" cols = "50" name="changedRule">
             <%
                 out.println(session.getAttribute("rule"));
             %>
         </textarea>
         <%
             out.println("<input type=\"hidden\" name=\"id\" value=\""+ session.getAttribute("ruleID") +"\">");
             out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
         %>
            <input type="submit" value="Change rule">

        </form>

</center>
</body>
</html>
