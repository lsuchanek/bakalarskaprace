package managers;

import enums.Age;
import enums.Sex;
import backend.User;
import common.DBUtils;
import common.IllegalEntityException;
import common.ServiceFailureException;

import java.util.List;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.sql.DataSource;
/**
 * Created by lsuchanek on 13.08.2018.
 */
public class UserManager {

    private static final Logger logger = Logger.getLogger(
            UserManager.class.getName());

    private DataSource dataSource;

    public UserManager(DataSource dataSource){
        this.dataSource = dataSource;
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    public void createUser(User user){
        checkDataSource();

        if(user.getId() != null){
            throw new IllegalEntityException("user id is not null");
        }

        try {
            Long id = getIdIfUserExists(user);
            if(id != null){
                user.setId(id);
                return;
            }
        }catch (SQLException ex){
            System.out.println(ex.getStackTrace());
        }

        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("INSERT INTO users (age, sex) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setString(1, user.getAge().toString());
            st.setString(2, user.getSex().toString());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,this,true);

            Long id = DBUtils.getId(st.getGeneratedKeys());
            user.setId(id);
            conn.commit();
        }catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while inserting user");

        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }

    }

    public Long getIdIfUserExists(User user) throws SQLException {
        checkDataSource();

        if(user.getId() != null){
            throw new IllegalEntityException("user id is not null");
        }
        Connection conn = null;
        PreparedStatement st = null;

            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM Users WHERE age = ? and sex = ?");
            st.setString(1,user.getAge().toString());
            st.setString(2, user.getSex().toString());
            user = executeQueryForSingleUser(st);
            if(user != null){
                return user.getId();
            }
            return null;

    }

    public void updateUser(User user){
        checkDataSource();
        if(user.getId() == null){
            throw new IllegalEntityException("user id is null");
        }
        Connection conn = null;
        PreparedStatement st = null;
        try{
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("UPDATE users SET age=? sex=? WHERE user_id=?");
            st.setString(1, user.getAge().toString());
            st.setString(2, user.getSex().toString());
            st.setLong(3, user.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, this, false);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while updating user");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public User getUser(Long id) throws ServiceFailureException {
        checkDataSource();
        if(id == null){
            throw new IllegalArgumentException("I can't find null user");
        }
        Connection conn = null;
        PreparedStatement st = null;

        try{
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM Users WHERE user_id = ?");
            st.setLong(1,id);
            return executeQueryForSingleUser(st);
        }catch (SQLException ex){
            throw new ServiceFailureException("Error while getting user with id: " + id + " from DB");
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    public List<User> listAllUsers() throws ServiceFailureException{
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Users");
            return executeQueryForMultipleUsers(statement);
        } catch (SQLException ex){
            throw new ServiceFailureException("Something went wrong with listing users");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }

    }

    static User executeQueryForSingleUser(PreparedStatement st)throws SQLException, ServiceFailureException {
        ResultSet result = st.executeQuery();
        if(result.next()){
            User user = rowToUser(result);
            if (result.next()){
                throw new ServiceFailureException("More users with same id");
            }
            return user;
        }else{
            return null;
        }
    }

    private static  List<User> executeQueryForMultipleUsers(PreparedStatement st) throws SQLException{
        ResultSet result = st.executeQuery();
        List<User> list = new ArrayList<>();
        while (result.next()){
            list.add(rowToUser(result));
        }
        return list;
    }

    static private User rowToUser(ResultSet rs) throws SQLException{
        Long id = rs.getLong("user_id");
        String ageString = rs.getString("age");
        Age age = stringToAge(ageString);
        String sexString = rs.getString("sex");
        Sex sex = stringToSex(sexString);
        User user = new User(age,sex);
        user.setId(id);

        return user;
    }

    private static Age stringToAge(String s){
        if(s.toLowerCase().equals("Teenage".toLowerCase())){
            return Age.TEENAGE;
        }
        if(s.toLowerCase().equals("Adult".toLowerCase())){
            return Age.ADULT;
        }
        if(s.toLowerCase().equals("Pensioner".toLowerCase())){
            return Age.PENSIONER;
        }
        return null;
    }

    private static Sex stringToSex(String s){
        if(s.toLowerCase().equals("Male".toLowerCase())){
            return Sex.MALE;
        }
        if(s.toLowerCase().equals("Female".toLowerCase())){
            return Sex.FEMALE;
        }
        return null;
    }
}
