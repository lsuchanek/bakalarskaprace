package managers;

import backend.Conversation;
import backend.Turn;
import backend.User;
import common.DBUtils;
import common.IllegalEntityException;
import common.ServiceFailureException;

import java.util.List;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.logging.Logger;
import javax.sql.DataSource;
/**
 * Created by lsuchanek on 16.08.2018.
 */
public class ConversationManager {
    private static final Logger logger = Logger.getLogger(
            ConversationManager.class.getName());

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public ConversationManager (DataSource dataSource){
        this.setDataSource(dataSource);
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    public void createConversation(Conversation conversation) throws IllegalEntityException, ServiceFailureException {
        checkDataSource();
        if (conversation.getId() != null) {
            throw new IllegalEntityException("conversation id is not null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("INSERT INTO conversation (user_id, active_conversation, has_wizard) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, conversation.getUser().getId());
            if(conversation.isActive())
                st.setString(2, "Y");
            else
                st.setString(2, "N");
            st.setString(3, "N");
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,conversation,true);
            conversation.setId(DBUtils.getId(st.getGeneratedKeys()));
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while inserting conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void updateConversation(Conversation conversation) throws IllegalEntityException, ServiceFailureException{
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("update conversation set user_id = ?, user_evaluation = ?, " +
                    "wizard_evaluation = ?, active_conversation = ?, has_wizard = ?" +
                    " where conversation_id = ? ");
            st.setLong(1, conversation.getUser().getId());
            st.setDouble(2, conversation.getUserEvaluation());
            st.setDouble(3, conversation.getWizardEvaluation());
            if(conversation.isActive())
                st.setString(4, "Y");
            else
                st.setString(4, "N");
            if(conversation.getWizard() == null)
                st.setString(5, "N");
            else
                st.setString(5,"Y");
            st.setLong(6, conversation.getId());
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while updating conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void endConversation(Conversation conversation){
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("update conversation set active_conversation=? " +
                    " where conversation_id = ? ");
            if(conversation.isActive())
                st.setString(1, "Y");
            else
                st.setString(1, "N");
            st.setLong(2, conversation.getId());
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while updating conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void updateUserEvaluation(Conversation conversation){
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("update conversation set user_evaluation = ? " +
                                            " where conversation_id = ? ");
            st.setDouble(1, conversation.getUserEvaluation());
            st.setLong(2, conversation.getId());
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while updating conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }

    }

    public void updateWizardEvaluation(Conversation conversation){
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("update conversation set wizard_evaluation = ? " +
                    " where conversation_id = ? ");
            st.setDouble(1, conversation.getWizardEvaluation());
            st.setLong(2, conversation.getId());
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while updating conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void updateCoversationWizardStatus(Conversation conversation){
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }
        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("update conversation set has_wizard = ?" +
                    " where conversation_id = ? ");
            if (conversation.getWizard() == null)
                st.setString(1, "N");
            else
                st.setString(1, "Y");
            st.setLong(2,conversation.getId());
            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());
            throw new ServiceFailureException("Error while updating conversation");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void deleteConversation(Conversation conversation) throws IllegalEntityException, ServiceFailureException{
        checkDataSource();
        if (conversation.getId() == null){
            throw new IllegalEntityException("conversation id is null");
        }
        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("DELETE FROM Conversation WHERE conversation_id=?");
            st.setLong(1, conversation.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, conversation, false);
            conn.commit();
        } catch (SQLException ex) {
            throw new ServiceFailureException("Error while updating customer");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public List<Conversation> listActiveConversations(){
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Conversation where active_conversation = ?");
            statement.setString(1, "Y");
            return executeQueryForMultipleConversations(statement);
        } catch (SQLException ex){
            System.err.println(ex.getMessage());
            throw new ServiceFailureException("Something went wrong with listing conversations");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }
    }

    public List<Conversation> listActiveWithoutWizard(){
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Conversation where active_conversation = ? and has_wizard = ?");
            statement.setString(1, "Y");
            statement.setString(2, "N");
            return executeQueryForMultipleConversations(statement);
        } catch (SQLException ex){
            System.err.println(ex.getMessage());
            throw new ServiceFailureException("Something went wrong with listing conversations");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }

    }

    public List<Conversation> listAllConversations() throws ServiceFailureException{
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Conversation");
            return executeQueryForMultipleConversations(statement);
        } catch (SQLException ex){
            throw new ServiceFailureException("Something went wrong with listing customers");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }
    }



    public Conversation getConversation(Long id) throws ServiceFailureException,IllegalArgumentException{
        checkDataSource();
        if(id == null){
            throw new IllegalArgumentException("I can't find null conversation");
        }
        Connection conn = null;
        PreparedStatement st = null;

        try {
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM Conversation WHERE conversation_id = ?");
            st.setLong(1,id);
            return executeQueryForSingleConversation(st);
        }catch (SQLException ex){
            ex.printStackTrace();
            throw new ServiceFailureException("Error while getting conversation with id: " + id + " from DB");
        } finally {
            DBUtils.closeQuietly(conn, st);
        }
    }

    private Conversation executeQueryForSingleConversation (PreparedStatement st) throws SQLException {
        ResultSet result = st.executeQuery();
        if(result.next()){
            Conversation conversation = this.rowToConversation(result);
            if (result.next()){
                throw new ServiceFailureException("More customers with same id");
            }
            return conversation;
        }else{
            return null;
        }
    }

    private Conversation rowToConversation(ResultSet rs) throws SQLException {
        Double userEval = rs.getDouble("user_evaluation");
        Double wizardEval = rs.getDouble("wizard_evaluation");
        Long userId = rs.getLong("user_id");
        UserManager manager = new UserManager(this.dataSource);
        User user = manager.getUser(userId);
        TurnManager turnManager = new TurnManager(this.dataSource);

        Conversation conversation = new Conversation(user);
        conversation.setId(rs.getLong("conversation_id"));
        conversation.setWizardEvaluation(wizardEval);
        conversation.setUserEvaluation(userEval);
        boolean active;
        if(rs.getString("active_conversation").equals("Y")){
            active = true;
        }else {
            active = false;
        }
        conversation.setActive(active);
        List<Turn> turns = turnManager.listTurnsOfConversation(conversation);
        conversation.setTurns(turns);

        return conversation;
    }

     private List<Conversation> executeQueryForMultipleConversations(PreparedStatement st) throws SQLException{
        ResultSet result = st.executeQuery();
        List<Conversation> conversationList = new ArrayList<>();
        while (result.next()){
            conversationList.add(rowToConversation(result));
        }
        return conversationList;
    }


}
