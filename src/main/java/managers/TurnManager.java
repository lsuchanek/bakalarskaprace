package managers;

import backend.Conversation;
import backend.Turn;
import common.DBUtils;
import common.IllegalEntityException;
import common.ServiceFailureException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

/**
 * Created by lsuchanek on 16.08.2018.
 */
public class TurnManager {
    private static final Logger logger = Logger.getLogger(
            TurnManager.class.getName());

    private DataSource dataSource;

    public TurnManager(DataSource dataSource){
        this.dataSource = dataSource;
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }
    public void createTurn(Turn turn, Conversation conversation) throws IllegalEntityException, ServiceFailureException {

        checkDataSource();

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("INSERT INTO Turns(conversation_id, question, user_initialized) VALUES (?,?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setLong(1, conversation.getId());
            st.setString(2, turn.getQuestion());
            if(turn.isUserInitialized()){
                st.setString(3, "Y");
            }else{
                st.setString(3, "N");
            }

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,turn,true);

            Long id = DBUtils.getId(st.getGeneratedKeys());
            turn.setId(id);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while inserting customer");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void updateTurnAnswer(Turn turn){
        checkDataSource();
        if(turn.getId() == null){
            throw new IllegalEntityException("turn id is null");
        }
        Connection conn = null;
        PreparedStatement st = null;
        try{
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("UPDATE turns SET answer=? WHERE turn_id=?");
            st.setString(1, turn.getReply());
            st.setLong(2, turn.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, this, false);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while updating turn");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void deleteTurn(Turn turn) throws IllegalEntityException, ServiceFailureException{

        checkDataSource();

        if(turn.getId() ==null){
            throw new IllegalEntityException("turn id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();

            conn.setAutoCommit(false);
            st = conn.prepareStatement("DELETE FROM Turns WHERE turn_id=?");
            st.setLong(1,turn.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, turn, false);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while deleting turn from db");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }


    public List<Turn> listTurnsOfConversation(Conversation conversation) throws ServiceFailureException{
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Turns where conversation_id = ?");
            statement.setLong(1, conversation.getId());
            return executeQueryForMultipleTurns(statement);
        } catch (SQLException ex){
            throw new ServiceFailureException("Something went wrong with listing turns");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }

    }


    public Turn getTurn(Long id) throws ServiceFailureException {
        checkDataSource();
        if(id == null){
            throw new IllegalArgumentException("I can't find null turn");
        }
        Connection conn = null;
        PreparedStatement st = null;

        try{
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM Turns WHERE turn_id = ?");
            st.setLong(1,id);
            return executeQueryForSingleTurn(st);
        }catch (SQLException ex){
            throw new ServiceFailureException("Error while getting turn with id: " + id + " from DB");
        } finally {
            DBUtils.closeQuietly(conn, st);
        }

    }
    static Turn executeQueryForSingleTurn(PreparedStatement st)throws SQLException, ServiceFailureException {
        ResultSet result = st.executeQuery();
        if(result.next()){
            Turn resultTurn = rowToTurn(result);
            if (result.next()){
                throw new ServiceFailureException("More customers with same id");
            }
            return resultTurn;
        }else{
            return null;
        }
    }

    static private Turn rowToTurn(ResultSet rs) throws SQLException{

        Turn result = new Turn();
        result.setId(rs.getLong("turn_id"));
        result.setQuestion(rs.getString("question"));
        result.setReply(rs.getString("answer"));
        if(rs.getString("user_initialized").equals("Y")){
            result.setUserInitialized(true);
        }else{
            result.setUserInitialized(false);
        }
        return result;
    }

    static private List<Turn> executeQueryForMultipleTurns(PreparedStatement st) throws SQLException{
        ResultSet result = st.executeQuery();
        List<Turn> turnList = new ArrayList<>();
        while (result.next()){
            turnList.add(rowToTurn(result));
        }
        return turnList;
    }
}
