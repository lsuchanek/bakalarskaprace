package enums;

/**
 * Created by lsuchanek on 06.08.2018.
 */
public enum Age {
    TEENAGE ("Teenage"),
    ADULT ("Adult"),
    PENSIONER ("Pensioner");

    private final String name;

    private Age(String s) {
        name = s;
    }

    public boolean equalsName(String otherName) {
        // (otherName == null) check is not needed because name.equals(null) returns false
        return name.equals(otherName);
    }

    public String toString() {
        return this.name;
    }
}
