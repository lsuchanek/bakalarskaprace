package servlets;


import enums.Role;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "DeleteAccountServlet",
        urlPatterns = "/DeleteAccount")
public class DeleteAccountServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String login = request.getParameter("delname");
        WizardManager.deleteWizard(login);

        RequestDispatcher view = request.getRequestDispatcher("admin.jsp");
        view.forward(request,response);
    }
}
