package servlets;

import backend.Conversation;
import backend.ConversationException;
import backend.PrepareDS;
import backend.User;
import xml.ConversationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "UserEndServlet",
            urlPatterns = "/UserEnd")
public class UserEndServlet extends HttpServlet {
    private static DataSource ds = PrepareDS.prepareDataSource();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        ConversationManager conversationManager = new ConversationManager(ds);
        String conIdStr = request.getParameter("conId");
        Long conId = Long.parseLong(conIdStr);
        Conversation conversation = ConversationManager.getConversation(conId);
        User user = conversation.getUser();
        user.setConversation(conversation);
        user.endConversation();

        String evalStr = request.getParameter("evaluation");
        evalStr = evalStr.replace(",", ".");
        Double evaluation = Double.parseDouble(evalStr);
        try {
            user.evaluate(evaluation);
        } catch (ConversationException e) {
            e.printStackTrace();
        }
        ConversationManager.endConversation(conId);
        RequestDispatcher view = request.getRequestDispatcher("userEnd.jsp");
        view.forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
