package servlets;

import backend.Conversation;
import backend.ConversationException;
import backend.PrepareDS;
import backend.Wizard;
import enums.Role;
import xml.ConversationManager;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "WizardEndServlet",
            urlPatterns = "/WizardEnd")
public class WizardEndServlet extends HttpServlet {
    private static DataSource ds = PrepareDS.prepareDataSource();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String conIdStr = request.getParameter("conId");
        Long conId = Long.parseLong(conIdStr);
        Conversation conversation = ConversationManager.getConversation(conId);
        Wizard wizard = new Wizard(conversation, null);
        wizard.endConversation();

        String evalStr = request.getParameter("evaluation");
        evalStr = evalStr.replace(",", ".");
        Double evaluation = Double.parseDouble(evalStr);
        try {
            wizard.evaluate(evaluation);
        } catch (ConversationException e) {
            e.printStackTrace();
        }

        ConversationManager.endConversation(conId);

        String login = (String) request.getSession().getAttribute("login");

        Role role = WizardManager.getRole(login);

        RequestDispatcher view = request.getRequestDispatcher("nonadmin.jsp");
        if (role.equals(Role.ADMIN))
            view = request.getRequestDispatcher("admin.jsp");

        view.forward(request,response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
