package servlets;

import backend.Conversation;
import backend.Wizard;
import enums.Role;
import xml.ConversationManager;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@WebServlet(name = "WizardJoinServlet",
        urlPatterns = "/JoinConversation")
public class WizardJoinServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getParameter("login");
        Role role = WizardManager.getRole(login);
        Wizard wizard;
        if (role.equals(Role.ADMIN)){
            wizard = new Wizard(login, true);
        }else {
            wizard = new Wizard(login, false);
        }
        List<Conversation> conversations = ConversationManager.getFreeConversations();
        if(conversations.size() > 0) {
            Conversation conversation = conversations.get(0);
            wizard.setConversation(conversation);
            ConversationManager.addWizardLogin(conversation.getId(),login);
            request.getSession().setAttribute("conId", conversation.getId());
            RequestDispatcher view = request.getRequestDispatcher("wizardConversation.jsp");
            view.forward(request,response);
        }else {
            RequestDispatcher view;
            request.setAttribute("noConversation", "yes");
            if(role.equals(Role.ADMIN)){

                view = request.getRequestDispatcher("admin.jsp");
            }else{
                view = request.getRequestDispatcher("nonadmin.jsp");
            }
            view.forward(request,response);
        }
    }
}
