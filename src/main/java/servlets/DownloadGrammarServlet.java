package servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.net.URISyntaxException;

@WebServlet(name = "DownloadGrammarServlet",
        urlPatterns = "/DownloadGrammar")
public class DownloadGrammarServlet extends HttpServlet {
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            File file = new File (DownloadServlet.class.getClassLoader().getResource("grammar.xml").toURI());

            response.setContentType("text/plain");
            response.setHeader("Content-disposition","attachment; filename=grammar.xml");

            OutputStream out = response.getOutputStream();
            FileInputStream in = new FileInputStream(file);
            byte[] buffer = new byte[4096];
            int length;
            while ((length = in.read(buffer)) > 0){
                out.write(buffer, 0, length);
            }
            in.close();
            out.flush();


        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }
}