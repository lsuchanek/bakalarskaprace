package servlets;

import backend.*;
import enums.Age;
import enums.Sex;
import managers.ConversationManager;
import managers.UserManager;
import org.xml.sax.SAXException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.URISyntaxException;

@WebServlet(name = "UserServlet",
        urlPatterns = "/User")


public class UserServlet extends HttpServlet {

//    private static DataSource ds = PrepareDS.prepareDataSource();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String ageString = request.getParameter("Age");
        String sexString = request.getParameter("Sex");

        Age age;
        if(ageString.equals("<20")){
            age = Age.TEENAGE;
        }else if(ageString.equals(">60")){
            age = Age.PENSIONER;
        }else{
            age = Age.ADULT;
        }

        Sex sex = Sex.valueOf(sexString);
        User user = new User(age, sex);

//        UserManager userManager = new UserManager(ds);
//        userManager.createUser(user);
//        ConversationManager conversationManager = new ConversationManager(ds);
        Conversation conversation = user.startConversation();

        //conversationManager.createConversation(conversation);

        request.getSession().setAttribute("conId", conversation.getId());

        RequestDispatcher view = request.getRequestDispatcher("userConversation.jsp");
        view.forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    private static void autoFill (Conversation conversation){
        Turn turn = new Turn();
        turn.setUserInitialized(true);
        turn.setQuestion("Ahoj");
        turn.setReply("Nazdar");
        conversation.addTurnToList(turn);
        turn = new Turn();
        turn.setUserInitialized(false);
        turn.setQuestion("Cau");
        turn.setReply("Mlask");
        conversation.addTurnToList(turn);
    }

}

