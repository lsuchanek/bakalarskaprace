package servlets;

import org.xml.sax.SAXException;
import xml.GrammarXml;
import xml.XmlToString;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by lsuchanek on 15.11.2018.
 */
@WebServlet(name = "ChooseRuleServlet",
        urlPatterns = "/RuleID")

public class ChooseRuleServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String ruleId = request.getParameter("id");
        String rule = XmlToString.ruleToString(ruleId);
        if(rule.isEmpty()){
            try {
                GrammarXml.addRule(ruleId, "", null);
            } catch (ParserConfigurationException e) {
                e.printStackTrace();
            } catch (SAXException e) {
                e.printStackTrace();
            } catch (TransformerException e) {
                e.printStackTrace();
            } catch (URISyntaxException e) {
                e.printStackTrace();
            }
        }
        rule = XmlToString.ruleToString(ruleId);

        request.getSession().setAttribute("rule", rule);
        request.getSession().setAttribute("ruleID", ruleId);
        RequestDispatcher view = request.getRequestDispatcher("wizardConversation.jsp");

        view.forward(request,response);
    }
}
