package servlets;

import backend.Conversation;
import backend.Wizard;
import xml.ConversationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Created by lsuchanek on 15.11.2018.
 */
@WebServlet(name = "AddRuleServelet",
        urlPatterns = "/AddRule")
public class AddRuleServelet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String conIdStr = request.getParameter("conId");
        Long conId = Long.parseLong(conIdStr);
        Conversation conversation = ConversationManager.getConversation(conId);
        Wizard wizard = new Wizard(conversation, null);

        String changedRule = request.getParameter("changedRule");

        wizard.addRule(changedRule);
//        GrammarToHtml.grammarConvert();

        RequestDispatcher view = request.getRequestDispatcher("wizardConversation.jsp");
        view.forward(request,response);

    }

}
