package servlets;

import xml.GrammarToHtml;
import xml.XmlToString;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChangeGrammarServelet",
        urlPatterns = "/ChangeGrammar")
public class ChangeGrammarServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException{
        String grammar = request.getParameter("changedGrammar");
        XmlToString.stringToGrammar(grammar);
        GrammarToHtml.grammarConvert();

        RequestDispatcher view = request.getRequestDispatcher("admin.jsp");
        view.forward(request,response);

    }

}
