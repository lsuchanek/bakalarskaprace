package servlets;

import backend.PrepareDS;
import backend.Wizard;
import enums.Role;
import xml.GrammarToHtml;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

/**
 * Created by lsuchanek on 15.11.2018.
 */
@WebServlet(name = "ChangeRuleServelet",
        urlPatterns = "/ChangeRule")
public class ChangeRuleServlet extends HttpServlet {
    private static DataSource ds = PrepareDS.prepareDataSource();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String login = request.getSession().getAttribute("login").toString();
        Role role = WizardManager.getRole(login);
        Wizard wizard = new Wizard(login, false);

        String changedRule = request.getParameter("changedRule");

        wizard.addRule(changedRule);
//        GrammarToHtml.grammarConvert();

        request.getSession().setAttribute("wizards", WizardManager.getAll());
        RequestDispatcher view = request.getRequestDispatcher("nonadmin.jsp");
        if(role.equals(Role.ADMIN)){
            view = request.getRequestDispatcher("admin.jsp");
        }
        view.forward(request,response);
    }

}
