package servlets;

import enums.Role;
import xml.GrammarToHtml;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "WizardLogInServlet",
        urlPatterns = "/WizardLogIn")

public class WizardLogInServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GrammarToHtml.grammarConvert();
        String pass = request.getParameter("pass");
        String login = request.getParameter("login");
        Role role = WizardManager.logIn(login, pass);


        if(role.equals(Role.DENY)){
            request.getSession().setAttribute("log", true);
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request,response);
            return;
        }else {
            request.getSession().setAttribute("wizards", WizardManager.getAll());
            request.getSession().setAttribute("log", false);
            request.getSession().setAttribute("login", login);
            RequestDispatcher view;
            if(role.equals(Role.ADMIN)){
                view = request.getRequestDispatcher("admin.jsp");
            }else{
                view = request.getRequestDispatcher("nonadmin.jsp");
            }
            view.forward(request,response);
            return;
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
