package servlets;

import xml.GrammarToHtml;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet(name = "WizardSignInServlet",
        urlPatterns = "/WizardSignIn")
public class WizardSignInServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        GrammarToHtml.grammarConvert();
        String pass = request.getParameter("pass");
        String login = request.getParameter("login");
        boolean success = WizardManager.signIn(login, pass);
        if(success){
            request.getSession().setAttribute("wizards", WizardManager.getAll());
            request.getSession().setAttribute("sign", false);
            request.getSession().setAttribute("login", login);
            RequestDispatcher view = request.getRequestDispatcher("nonadmin.jsp");
            view.forward(request,response);
        }else {
            request.getSession().setAttribute("sign", true);
            RequestDispatcher view = request.getRequestDispatcher("index.jsp");
            view.forward(request,response);
        }

    }
}
