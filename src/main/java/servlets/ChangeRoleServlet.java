package servlets;


import backend.Wizard;
import enums.Role;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChangeRoleServelet",
        urlPatterns = "/ChangeRole")
public class ChangeRoleServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String account = request.getParameter("account");
        Role role = WizardManager.getRole(account);
        if(role.equals(Role.ADMIN)){
            WizardManager.changeRole(account, false);
        }else {
            WizardManager.changeRole(account, true);
        }
        request.getSession().setAttribute("wizards", WizardManager.getAll());
        RequestDispatcher view = request.getRequestDispatcher("admin.jsp");
        view.forward(request,response);

    }

}
