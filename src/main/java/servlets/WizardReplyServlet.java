package servlets;

import backend.*;
import xml.ConversationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "WizardReplyServlet",
            urlPatterns = "/WizardReply")
public class WizardReplyServlet extends HttpServlet {

    private static DataSource ds = PrepareDS.prepareDataSource();
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String conIdStr = request.getParameter("conId");
        Long conId = Long.parseLong(conIdStr);
        Conversation conversation = ConversationManager.getConversation(conId);
        String login = request.getSession().getAttribute("login").toString();
        Wizard wizard = new Wizard(conversation, login,false);
        String reply = request.getParameter("answer");
        try {
            wizard.reply(reply);
        } catch (ConversationException e) {
            request.getSession().setAttribute("conId", conId);
            RequestDispatcher view = request.getRequestDispatcher("conversationClosedWizard.jsp");
            view.forward(request,response);
            return;
        }
//        String conIdStr = request.getParameter("conId");
//        Long conId = Long.parseLong(conIdStr);
//        Conversation conversation = ConversationManager.getConversation(conId);
//
//        User user = conversation.getUser();
//        user.setConversation(conversation);
////        TurnManager turnManager = new TurnManager(ds);
//        String reply = request.getParameter("answer");
//        try {
//            user.reply(reply);
//        } catch (ConversationException e) {
////                request.getSession().setAttribute("conMan", conversationManager);
//            request.getSession().setAttribute("conId", conId);
//            RequestDispatcher view = request.getRequestDispatcher("conversationClosedUser.jsp");
//            view.forward(request,response);
//            return;
//        }
//        request.getSession().setAttribute("conMan", conversationManager);
        request.getSession().setAttribute("conId", conId);

        RequestDispatcher view = request.getRequestDispatcher("wizardConversation.jsp");
        view.forward(request,response);

    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
