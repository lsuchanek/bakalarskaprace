package servlets;

import backend.Conversation;
import backend.ConversationException;
import backend.PrepareDS;
import backend.User;
import xml.ConversationManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;

@WebServlet(name = "UserReplyServlet",
            urlPatterns = "/UserReply"
)
public class UserReplyServlet extends HttpServlet {

    private static DataSource ds = PrepareDS.prepareDataSource();


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        ConversationManager conversationManager = new ConversationManager(ds);
        String conIdStr = request.getParameter("conId");
        Long conId = Long.parseLong(conIdStr);
        Conversation conversation = ConversationManager.getConversation(conId);

        User user = conversation.getUser();
        user.setConversation(conversation);
//        TurnManager turnManager = new TurnManager(ds);
        String reply = request.getParameter("answer");
            try {
                user.reply(reply);
            } catch (ConversationException e) {
//                request.getSession().setAttribute("conMan", conversationManager);
                request.getSession().setAttribute("conId", conId);
                RequestDispatcher view = request.getRequestDispatcher("conversationClosedUser.jsp");
                view.forward(request,response);
                return;
            }

//        request.getSession().setAttribute("conMan", conversationManager);
        request.getSession().setAttribute("conId", conId);
//            request.getSession().setAttribute("conversation", conversation);
        request.setAttribute("convID", conId);
        RequestDispatcher view = request.getRequestDispatcher("userConversation.jsp");
        view.forward(request,response);


    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
