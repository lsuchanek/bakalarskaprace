package servlets;

import enums.Role;
import xml.WizardManager;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = "ChangePasswordServelet",
        urlPatterns = "/ChangePassword")
public class ChangePasswordServlet extends HttpServlet {

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String login = request.getSession().getAttribute("login").toString();
        Role role = WizardManager.getRole(login);
        String password = request.getParameter("newPassword");
        WizardManager.changePassword(login,password);

        RequestDispatcher view = request.getRequestDispatcher("nonadmin.jsp");
        if(role.equals(Role.ADMIN)){
            view = request.getRequestDispatcher("admin.jsp");
        }
        view.forward(request,response);

    }
}
