package backend;

import xml.ConversationManager;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuchanek on 06.08.2018.
 */
public class Conversation {
    private Long id;
    private User user;
    private Wizard wizard;
    private List<Turn> turns;
//    private Turn actualTurn;
    private Double userEvaluation;
    private Double wizardEvaluation;
    private boolean active;

    public Conversation(User user){
        this.setUser(user);
//        this.setActualTurn(null);
        this.setActive(true);
        turns = new ArrayList<>();
    }
    public Conversation(){
        turns = new ArrayList<>();
    }

    public void addTurnToList(Turn turn){
        turns.add(turn);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Wizard getWizard() {
        return wizard;
    }

    public void setWizard(Wizard wizard) {
        this.wizard = wizard;
        ConversationManager.addWizard(this.getId());
    }

    public List<Turn> getTurns() {
        return turns;
    }

    public void setTurns(List<Turn> turns) {
        this.turns = turns;
    }

    public Double getUserEvaluation() {
        return userEvaluation;
    }

    public void setUserEvaluation(Double userEvaluation) {
        this.userEvaluation = userEvaluation;
    }

    public Double getWizardEvaluation() {
        return wizardEvaluation;
    }

    public void setWizardEvaluation(Double wizardEvaluation) {
        this.wizardEvaluation = wizardEvaluation;
    }

//    public Turn getActualTurn() {
//        return actualTurn;
//    }

//    public void setActualTurn(Turn actualTurn) {
//        this.actualTurn = actualTurn;
//    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void printList(){
            for(Turn turn: turns){
                if(turn.isUserInitialized())
                    System.out.println("U");
                else
                    System.out.println("W");
                System.out.println("Q: " + turn.getQuestion());
                System.out.println("A: " + turn.getReply());
            }
    }

    public String toString(){
        return "ID: " + id;
    }
}
