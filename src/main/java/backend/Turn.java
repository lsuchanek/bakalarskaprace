package backend;

/**
 * Created by lsuchanek on 06.08.2018.
 */
public class Turn {
    private Long id;
    private String question = new String();
    private String reply = new String();
    private boolean userInitialized;

    public Turn(String question, boolean userInitialized){
        this.setQuestion(question);
        this.setUserInitialized(userInitialized);
    }

    public Turn(){}

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getReply() {
        return reply;
    }

    public void setReply(String reply) {
        this.reply = reply;
    }

    public boolean isUserInitialized() {
        return userInitialized;
    }

    public void setUserInitialized(boolean userInitialized) {
        this.userInitialized = userInitialized;
    }
}
