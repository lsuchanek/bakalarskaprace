package backend;


import enums.Age;
import enums.Sex;
import xml.ConversationManager;

/**
 * Created by lsuchanek on 06.08.2018.
 */
public class User {
    private Long id;
    private Age age;
    private Sex sex;
    private Conversation conversation;

    public User(Age age, Sex sex){
        this.setAge(age);
        this.setSex(sex);
    }

    public void reply(String reply) throws ConversationException {
        if(!conversation.isActive()){
            throw new ConversationException("Can't reply to closed conversation");
        }
        Turn turn;
        if(conversation.getTurns().size() == 0){
            turn = new Turn(reply, true);
            conversation.addTurnToList(turn);
            try {
                ConversationManager.addTurn(conversation.getId(), turn);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            turn = conversation.getTurns().get(conversation.getTurns().size() - 1);
            if (turn.getReply() == null || turn.getReply().equals("")) {
                turn.setReply(reply);
                try {
                    ConversationManager.addAnswer(conversation.getId(), turn);
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                turn = new Turn(reply, true);
                conversation.addTurnToList(turn);
                try {
                    ConversationManager.addTurn(conversation.getId(), turn);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

    }

    public Conversation startConversation(){
        Conversation conversation = new Conversation(this);
        this.setConversation(conversation);
        try {
            long id = ConversationManager.newConversation(this);
            conversation.setId(id);
            return conversation;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public boolean endConversation() {
        conversation.setActive(false);
        try {
            ConversationManager.endConversation(conversation.getId());
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public void evaluate(Double evaluation) throws ConversationException {
        try {
            ConversationManager.addEval(conversation.getId(), evaluation, true);
        }catch (Exception e){
            e.printStackTrace();
        }
        conversation.setUserEvaluation(evaluation);
    }

    public Age getAge() {
        return age;
    }

    public void setAge(Age age) {
        this.age = age;
    }

    public Sex getSex() {
        return sex;
    }

    public void setSex(Sex sex) {
        this.sex = sex;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
