package backend;

import common.DBUtils;
import common.IllegalEntityException;
import common.ServiceFailureException;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;


/**
 * Created by lsuchanek on 16.08.2018.
 */
public class GrammarManager {

    private static final Logger logger = Logger.getLogger(
            GrammarManager.class.getName());

    private DataSource dataSource;

    public void setDataSource(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    public GrammarManager(DataSource dataSource){
        this.dataSource = dataSource;
    }

    private void checkDataSource() {
        if (dataSource == null) {
            throw new IllegalStateException("DataSource is not set");
        }
    }

    public void createGrammar(Grammar grammar, Conversation conversation) throws IllegalEntityException, ServiceFailureException {

        checkDataSource();

        if(grammar.getId() != null){
            throw new IllegalEntityException("grammar id is not null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();
            conn.setAutoCommit(false);
            st = conn.prepareStatement("INSERT INTO Grammar( conversation_name, conversation_path) VALUES (?,?)",
                    Statement.RETURN_GENERATED_KEYS);
            st.setString(1, grammar.getName());
            st.setString(2, grammar.getPath());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count,grammar,true);

            Long id = DBUtils.getId(st.getGeneratedKeys());
            grammar.setId(id);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while inserting grammar");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public void deleteGrammar(Grammar grammar) throws IllegalEntityException, ServiceFailureException{

        checkDataSource();

        if(grammar.getId() ==null){
            throw new IllegalEntityException("grammar id is null");
        }

        Connection conn = null;
        PreparedStatement st = null;
        try {
            conn = dataSource.getConnection();

            conn.setAutoCommit(false);
            st = conn.prepareStatement("DELETE FROM Grammar WHERE grammar_id=?");
            st.setLong(1, grammar.getId());

            int count = st.executeUpdate();
            DBUtils.checkUpdatesCount(count, grammar, false);
            conn.commit();
        }catch (SQLException ex) {
            throw new ServiceFailureException("Error while deleting grammar from db");
        } finally {
            DBUtils.doRollbackQuietly(conn);
            DBUtils.closeQuietly(conn, st);
        }
    }

    public List<Grammar> listAllGrammars() throws ServiceFailureException{
        checkDataSource();
        Connection conn = null;
        PreparedStatement statement = null;
        try {
            conn = dataSource.getConnection();
            statement = conn.prepareStatement("SELECT * FROM Grammar");
            return executeQueryForMultipleGrammars(statement);
        } catch (SQLException ex){
            throw new ServiceFailureException("Something went wrong with listing grammars");
        } finally{
            DBUtils.closeQuietly(conn, statement);
        }

    }

    public Grammar getGrammar(Long id) throws ServiceFailureException {
        checkDataSource();
        if(id == null){
            throw new IllegalArgumentException("I can't find null grammar");
        }
        Connection conn = null;
        PreparedStatement st = null;

        try{
            conn = dataSource.getConnection();
            st = conn.prepareStatement("SELECT * FROM Grammar WHERE grammar_id = ?");
            st.setLong(1,id);
            return executeQueryForSingleGrammar(st);
        }catch (SQLException ex){
            throw new ServiceFailureException("Error while getting grammar with id: " + id + " from DB");
        } finally {
            DBUtils.closeQuietly(conn, st);
        }

    }

    static Grammar executeQueryForSingleGrammar(PreparedStatement st)throws SQLException, ServiceFailureException {
        ResultSet result = st.executeQuery();
        if(result.next()){
            Grammar grammar = rowToGrammar(result);
            if (result.next()){
                throw new ServiceFailureException("More Grammars with same id");
            }
            return grammar;
        }else{
            return null;
        }
    }
    static private List<Grammar> executeQueryForMultipleGrammars(PreparedStatement st) throws SQLException{
        ResultSet result = st.executeQuery();
        List<Grammar> grammarList = new ArrayList<>();
        while (result.next()){
            grammarList.add(rowToGrammar(result));
        }
        return grammarList;
    }

    static Grammar rowToGrammar(ResultSet rs) throws SQLException {
        Grammar grammar = new Grammar();
        grammar.setId(rs.getLong("grammar_id"));
        grammar.setName(rs.getString("grammar_name"));
        grammar.setPath(rs.getString("grammar_path"));
        return grammar;
    }

}
