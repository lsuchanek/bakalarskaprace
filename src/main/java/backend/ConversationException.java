package backend;

/**
 * Created by lsuchanek on 08.08.2018.
 */
public class ConversationException extends Exception {

    public ConversationException (String s){
        super(s);
    }
}
