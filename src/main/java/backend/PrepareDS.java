package backend;

import common.DBUtils;
import org.apache.derby.jdbc.EmbeddedDataSource;

import javax.sql.DataSource;
import java.sql.SQLException;

public class PrepareDS {

    public static DataSource prepareDataSource() {

       EmbeddedDataSource ds = new EmbeddedDataSource();

        ds.setDatabaseName("conversations");

        ds.setCreateDatabase("create");
        try {
            DBUtils.tryCreateTables(ds, Main.class.getClassLoader().getResource("createTables.sql"));
        }catch (SQLException ex){


        }

        return ds;

    }
}
