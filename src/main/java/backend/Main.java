package backend;

import common.DBUtils;

import javax.sql.DataSource;

import java.io.IOException;
import java.net.URISyntaxException;
import java.sql.SQLException;

/**
 * Created by lsuchanek on 08.08.2018.
 */
public class Main {

    private static DataSource ds;

    public static void main(String[] args) throws ConversationException, SQLException, URISyntaxException, IOException {
        ds = PrepareDS.prepareDataSource();
        DBUtils.executeSqlScript(ds, Main.class.getClassLoader().getResource("dropTables.sql"));
        ds = PrepareDS.prepareDataSource();
    }

    private static void dropTables(DataSource ds) throws SQLException {
        DBUtils.executeSqlScript(ds, Main.class.getResource("dropTables.sql"));
    }

}
