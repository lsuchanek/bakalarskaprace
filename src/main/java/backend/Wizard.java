package backend;

import xml.ConversationManager;
import xml.GrammarToHtml;
import xml.XmlToString;

import java.io.Serializable;

/**
 * Created by lsuchanek on 06.08.2018.
 */
public class Wizard implements Serializable {

    private Conversation conversation;
    private String login;
    private boolean admin;

    public Wizard (Conversation conversation, Grammar grammar){
        this.setConversation(conversation);
        conversation.setWizard(this);
    }

    public Wizard(Conversation conversation, String login, boolean admin){
        this.setAdmin(admin);
        this.setLogin(login);
        this.setConversation(conversation);
    }

    public Wizard( String login, boolean admin){
        this.setAdmin(admin);
        this.setLogin(login);
    }

    public void reply(String reply) throws ConversationException {
        if(!conversation.isActive()){
            throw new ConversationException("Can't reply to closed conversation");
        }
        Turn turn;
        if(conversation.getTurns().size() == 0){
            turn = new Turn(reply, false);
            conversation.addTurnToList(turn);
            try {
                ConversationManager.addTurn(conversation.getId(), turn);
            }catch (Exception e){
                e.printStackTrace();
            }
        }else {
            turn = conversation.getTurns().get(conversation.getTurns().size() - 1);
            if (turn.getReply() == null || turn.getReply().equals("")) {
                turn.setReply(reply);
                try {
                    ConversationManager.addAnswer(conversation.getId(), turn);
                }catch (Exception e){
                    e.printStackTrace();
                }
            } else {
                turn = new Turn(reply, false);
                conversation.addTurnToList(turn);
                try {
                    ConversationManager.addTurn(conversation.getId(), turn);
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }

}

    public boolean endConversation() {

        conversation.setActive(false);
        try {
            ConversationManager.endConversation(conversation.getId());
        }catch (Exception e){
            e.printStackTrace();
        }

        return true;
    }

    public void evaluate(Double evaluation) throws ConversationException {
        try {
            ConversationManager.addEval(conversation.getId(), evaluation, false);
        }catch (Exception e){
            e.printStackTrace();
        }
        conversation.setUserEvaluation(evaluation);
    }

    public void addRule(String value){
        XmlToString.stringToRule(value);
        GrammarToHtml.grammarConvert();
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public Conversation getConversation() {
        return conversation;
    }

    public void setConversation(Conversation conversation) {
        this.conversation = conversation;
        conversation.setWizard(this);
    }

}
