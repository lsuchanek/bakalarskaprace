package common;

/**
 * Created by Lenovo on 13. 3. 2017.
 */
public class IllegalEntityException extends RuntimeException {

    public IllegalEntityException() {
    }

    /**
     * @param msg the detail message.
     */
    public IllegalEntityException(String msg) {
        super(msg);
    }

    /**
     * @param message the detail message.
     * @param cause the cause
     */
    public IllegalEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
