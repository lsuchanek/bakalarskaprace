package common;

/**
 * Created by Lenovo on 13. 3. 2017.
 */
public class ValidationException extends RuntimeException {
    public ValidationException() {
    }

    public ValidationException(String msg) {
        super(msg);
    }
}
