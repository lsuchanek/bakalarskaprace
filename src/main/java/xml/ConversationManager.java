package xml;

import java.io.File;

import backend.*;
import enums.Age;
import enums.Sex;
import org.w3c.dom.*;

import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by lsuchanek on 10.10.2018.
 */
public class ConversationManager {

    /*public static void main(String[] args) throws ConversationException {
        User user = new User(Age.TEENAGE, Sex.FEMALE);
        Conversation con = user.startConversation();
        Wizard wizard = new Wizard(con, "admin", true);
        Wizard wizard1 = new Wizard(con, "admin", true);
        user.reply("Ahoj");
        wizard.reply("Hi");
        user.reply("hello");
        user.evaluate(1.5);
        wizard.evaluate(1.8);

        wizard.endConversation();
        user.endConversation();
//        addWizard(con.getId(), "admin");
        Conversation conversation = getConversation(con.getId());
        System.out.println(conversation.getTurns().size());

    }*/
    public static void addConversation(Conversation conversation){
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI()/*".\\src\\main\\resources\\conversations.xml"*/);
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();
            Element newConversation = doc.createElement("conversation");
            Element userSex = doc.createElement("sex");
            Element userAge = doc.createElement("age");
            Element userEval = doc.createElement("userEvaluation");
            Element wizardEval = doc.createElement("wizardEvaluation");
            userSex.appendChild(doc.createTextNode(conversation.getUser().getSex().toString()));
            userAge.appendChild(doc.createTextNode(conversation.getUser().getAge().toString()));
            userEval.appendChild(doc.createTextNode(conversation.getUserEvaluation().toString()));
            wizardEval.appendChild(doc.createTextNode(conversation.getWizardEvaluation().toString()));
            newConversation.appendChild(userSex);
            newConversation.appendChild(userAge);
            newConversation.appendChild(userEval);
            newConversation.appendChild(wizardEval);
            for(Turn turn: conversation.getTurns()){
                Element newTurn = doc.createElement("turn");

                if(turn.isUserInitialized())
                    newTurn.setAttribute("initialized", "user");
                else
                    newTurn.setAttribute("initialized", "wizard");

                Element question = doc.createElement("question");
                question.appendChild(doc.createTextNode(turn.getQuestion()));
                Element answer = doc.createElement("answer");
                if(turn.getReply()!= null){
                    answer.appendChild(doc.createTextNode(turn.getReply()));
                }
                newTurn.appendChild(question);
                newTurn.appendChild(answer);
                newConversation.appendChild(newTurn);
            }
            newConversation.setAttribute("id", conversation.getId().toString());
            root.appendChild(newConversation);
            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static long newConversation(User user) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();
            Element newConversation = doc.createElement("conversation");
            Element userSex = doc.createElement("sex");
            Element userAge = doc.createElement("age");
            userSex.appendChild(doc.createTextNode(user.getSex().toString()));
            userAge.appendChild(doc.createTextNode(user.getAge().toString()));
            NodeList nodeList = doc.getElementsByTagName("conversation");
            long id = nodeList.getLength() + 1;
            newConversation.setAttribute("id", String.valueOf(id));
            newConversation.setAttribute("active", "1");
            newConversation.setAttribute("wizard", "1");
            newConversation.appendChild(userSex);
            newConversation.appendChild(userAge);
            root.appendChild(newConversation);
            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);

            return id;
        }catch (Exception e){
            e.printStackTrace();
        }
        return 0;
    }

    public static void addEval(long conId, double eval, boolean user) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();
            NodeList nodeList = doc.getElementsByTagName("conversation");
            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(conId))){
                        if(user){
                            Element evaluation = doc.createElement("userEvaluation");
                            evaluation.appendChild(doc.createTextNode(String.valueOf(eval)));
                            el.appendChild(evaluation);
                        } else{
                            Element evaluation = doc.createElement("wizardEvaluation");
                            evaluation.appendChild(doc.createTextNode(String.valueOf(eval)));
                            el.appendChild(evaluation);
                        }
                    }
                }
            }
            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void addTurn(long id, Turn turn){
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");
            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
                        Element newTurn = doc.createElement("turn");
                        Element question = doc.createElement("question");
                        question.appendChild(doc.createTextNode(turn.getQuestion()));
                        newTurn.appendChild(question);
                        if(turn.isUserInitialized())
                            newTurn.setAttribute("initialized", "user");
                        else
                            newTurn.setAttribute("initialized", "wizard");
                        el.appendChild(newTurn);
                    }
                }
            }
            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void addAnswer(long id, Turn turn) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");

            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
                        NodeList turns = el.getElementsByTagName("turn");
                        Element last = (org.w3c.dom.Element) turns.item(turns.getLength()-1);
                        Element answer = doc.createElement("answer");
                        answer.appendChild(doc.createTextNode(turn.getReply()));
                        last.appendChild(answer);
                    }
                }
            }

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static Conversation getConversation(long id){

        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");

            Conversation conversation = new Conversation();
            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
                        conversation = nodeToConversation(el);
                    }
                }
            }
            return conversation;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static void endConversation(long id){

        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");

            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
                        el.removeAttribute("active");
                    }
                }
            }

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public static void addWizard(long id) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");

            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
                        el.removeAttribute("wizard");
//                        Element wizard = doc.createElement("wizard");
//                        wizard.appendChild(doc.createTextNode(login));
//                        el.insertBefore(wizard, el.getFirstChild());
                    }
                }
            }

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }
    public static void addWizardLogin(long id, String login) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");

            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("id") && el.getAttribute("id").equals(String.valueOf(id))){
//                        el.removeAttribute("wizard");
                        Element wizard = doc.createElement("wizard");
                        wizard.appendChild(doc.createTextNode(login));
                        el.insertBefore(wizard, el.getFirstChild());
                    }
                }
            }

            DOMSource source = new DOMSource(doc);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("conversations.xml").getPath()/*".\\src\\main\\resources\\conversations.xml"*/);
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static List<Conversation> getFreeConversations() {

        try {
            File file = new File(Main.class.getClassLoader().getResource("conversations.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList nodeList = doc.getElementsByTagName("conversation");
            List<Conversation> conversations = new ArrayList<>();
            if (nodeList != null && nodeList.getLength() > 0) {
                for (int i = 0; i < nodeList.getLength(); i++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(i);
                    if(el.hasAttribute("active")&& el.hasAttribute("wizard")){
                        Conversation conversation = nodeToConversation(el);
                        conversations.add(conversation);
                    }
                }
            }
            return conversations;
        }catch (Exception e){
            e.printStackTrace();
        }

        return null;
    }

    private static Conversation nodeToConversation(Element el){
        Conversation conversation = new Conversation();

        NodeList turns = el.getElementsByTagName("turn");
        conversation.setId(Long.parseLong(el.getAttribute("id")));
        if(el.hasAttribute("active")){
            conversation.setActive(true);
        }else {
            conversation.setActive(false);
        }
        NodeList age = el.getElementsByTagName("age");
        NodeList sex = el.getElementsByTagName("sex");
        Node a = age.item(0);
        Node s = sex.item(0);
        String aString = a.getFirstChild().getTextContent();
        String sString = s.getFirstChild().getTextContent();
        Age userAge = stringToAge(aString);
        Sex userSex = stringToSex(sString);
        User user = new User(userAge, userSex);
        conversation.setUser(user);
        for(int j = 0; j <turns.getLength(); j++){
            Element turn = (org.w3c.dom.Element) turns.item(j);
            Turn t = new Turn();
            if(turn.getAttribute("initialized").equals("user"))
                t.setUserInitialized(true);
            else
                t.setUserInitialized(false);

            NodeList question = turn.getElementsByTagName("question");
            Node q = question.item(0);
            NodeList answer = turn.getElementsByTagName("answer");
            a = answer.item(0);
            t.setQuestion(q.getFirstChild().getTextContent());
            if(a != null)
                t.setReply(a.getFirstChild().getTextContent());
            else
                t.setReply(null);
            conversation.addTurnToList(t);
        }

        return conversation;
    }

    private static Age stringToAge(String s){
        if(s.toLowerCase().equals("Teenage".toLowerCase())){
            return Age.TEENAGE;
        }
        if(s.toLowerCase().equals("Adult".toLowerCase())){
            return Age.ADULT;
        }
        if(s.toLowerCase().equals("Pensioner".toLowerCase())){
            return Age.PENSIONER;
        }
        return null;
    }

    private static Sex stringToSex(String s){
        if(s.toLowerCase().equals("Male".toLowerCase())){
            return Sex.MALE;
        }
        if(s.toLowerCase().equals("Female".toLowerCase())){
            return Sex.FEMALE;
        }
        return null;
    }
}
