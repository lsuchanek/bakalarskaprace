package xml;

import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;

import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.TransformerFactoryConfigurationError;
import javax.xml.transform.stream.StreamResult;
import javax.xml.transform.stream.StreamSource;

public class GrammarToHtml {

//    public static void main(String[] args) {
//        grammarConvert();
//    }

    public static void grammarConvert(){
        Source xml = new StreamSource(GrammarToHtml.class.getClassLoader().getResourceAsStream("grammar.xml"));
        Source xslt = new StreamSource(GrammarToHtml.class.getClassLoader().getResourceAsStream("grammar.xsl"));

        convert(xml, xslt);
    }

    public static void convert(Source xml, Source xslt){

        StringWriter sw = new StringWriter();

        try {

            FileWriter fw = new FileWriter(GrammarToHtml.class.getClassLoader().getResource("grammar.html").getPath());
            TransformerFactory tFactory = TransformerFactory.newInstance();
            Transformer transform = tFactory.newTransformer(xslt);
            transform.transform(xml, new StreamResult(sw));
            fw.write(sw.toString());
            fw.close();

            System.out
                    .println("grammar.html generated successfully");

        } catch (IOException | TransformerConfigurationException e) {
            e.printStackTrace();
        } catch (TransformerFactoryConfigurationError e) {
            e.printStackTrace();
        } catch (TransformerException e) {
            e.printStackTrace();
        }

    }
}
