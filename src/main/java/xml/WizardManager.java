package xml;

import backend.Main;
import backend.Wizard;
import enums.Role;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

public class WizardManager {

    public static void main(String[] args) {
        changePassword("abc", "cba");

    }

    public static boolean signIn(String login, String password){
        try {
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if(log.getFirstChild().getTextContent().equals(login)){
                    return false;
                }
            }


            Element wizard = doc.createElement("wizard");
            wizard.setAttribute("role", "normal");

            Element name = doc.createElement("login");
            name.appendChild(doc.createTextNode(login));
            Element pass = doc.createElement("password");

            String myHash = hashPassword(password);
            pass.appendChild(doc.createTextNode(myHash));
            wizard.appendChild(name);
            wizard.appendChild(pass);
            root.appendChild(wizard);

            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("wizards.xml").getPath());
            transformer.transform(source, result);
            return true;

        }catch (Exception e){
            e.printStackTrace();
        }

        return false;
    }

    public static Role logIn(String login, String password){
        try {
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if(log.getFirstChild().getTextContent().equals(login)){
                    String myHash = hashPassword(password);
                    NodeList passList = el.getElementsByTagName("password");
                    Node pass = passList.item(0);

                    if (myHash.equals(pass.getFirstChild().getTextContent())){
                        if(el.getAttribute("role").equals("admin")){
                            return Role.ADMIN;
                        }
                        return Role.NORMAL;
                    }
                }
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return Role.DENY;
    }

    public static Role getRole(String login){
        try {
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if(log.getFirstChild().getTextContent().equals(login)){
                    if(el.getAttribute("role").equals("admin")){
                        return Role.ADMIN;
                    }
                    return Role.NORMAL;
                }
            }

        }catch (Exception e){
            e.printStackTrace();
        }
        return Role.DENY;
    }

    public static void changeRole(String login, boolean promote){
        try {
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if(log.getFirstChild().getTextContent().equals(login)){
                    if(promote){
                        el.setAttribute("role", "admin");
                    }else {
                        el.setAttribute("role", "normal");
                    }
                }
            }

            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("wizards.xml").getPath());
            transformer.transform(source, result);

        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void changePassword(String login, String newPassword){

        try {
            String hashPassword = hashPassword(newPassword);
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if(log.getFirstChild().getTextContent().equals(login)){
                    el.removeChild(el.getElementsByTagName("password").item(0));
                    Element password = doc.createElement("password");
                    password.appendChild(doc.createTextNode(hashPassword));
                    el.appendChild(password);
                }
            }

            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("wizards.xml").getPath());
            transformer.transform(source, result);

        }catch (Exception e){
            e.printStackTrace();
        }


    }

    public static void deleteWizard(String login){
        if(login.equals("admin")){
            return;
        }
        try {
            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for (int i = 0; i < wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                NodeList nameList = el.getElementsByTagName("login");
                Node log = nameList.item(0);
                if (log.getFirstChild().getTextContent().equals(login)) {
                    el.getParentNode().removeChild(el);
                }

            }
            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("wizards.xml").getPath());
            transformer.transform(source, result);

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    public static List<Wizard> getAll(){
        try {
            List<Wizard> wizardList = new ArrayList<>();

            File file = new File(Main.class.getClassLoader().getResource("wizards.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            NodeList wizards = root.getElementsByTagName("wizard");

            for(int i = 0; i< wizards.getLength(); i++) {
                Element el = (org.w3c.dom.Element) wizards.item(i);
                String login = el.getElementsByTagName("login").item(0).getFirstChild().getTextContent();
                if(el.getAttribute("role").equals("admin")){
                    Wizard wizard = new Wizard(login, true);
                    wizardList.add(wizard);
                }else {
                    Wizard wizard = new Wizard(login, false);
                    wizardList.add(wizard);
                }
            }

            return wizardList;
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static String hashPassword(String password) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("MD5");
        md.update(password.getBytes());
        byte[] digest = md.digest();
        String myHash = DatatypeConverter.printHexBinary(digest).toUpperCase();
        return myHash;
    }
}
