package xml;

import backend.Main;
import org.w3c.dom.*;
import org.w3c.dom.ls.DOMImplementationLS;
import org.w3c.dom.ls.LSSerializer;
import org.xml.sax.InputSource;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.StringReader;
import java.io.StringWriter;

public class XmlToString {

    public static void main(String[] args) {

        System.out.println(ruleToString("city"));
    }

    public static String ruleToString(String id) {
        try {
            File file = new File(Main.class.getClassLoader().getResource("grammar.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();
            StringWriter writer = new StringWriter();
            NodeList nodeList = doc.getElementsByTagName("rule");
            if (nodeList != null && nodeList.getLength() > 0) {
                for (int j = 0; j < nodeList.getLength(); j++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(j);
                    if (el.getAttribute("id").equals(id)) {
                        Transformer trans = TransformerFactory.newInstance().newTransformer();
                        // @checkstyle MultipleStringLiterals (1 line)
                        trans.setOutputProperty(OutputKeys.INDENT, "yes");
                        trans.setOutputProperty(OutputKeys.VERSION, "1.0");
                        trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
                        trans.transform(new DOMSource(el), new StreamResult(writer));
                    }
                }
            }
            return writer.toString();
        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    public static String grammarToString(){
        try {
            File file = new File(Main.class.getClassLoader().getResource("grammar.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();
            StringWriter writer = new StringWriter();

            Transformer trans = TransformerFactory.newInstance().newTransformer();
            // @checkstyle MultipleStringLiterals (1 line)
            trans.setOutputProperty(OutputKeys.INDENT, "yes");
            trans.setOutputProperty(OutputKeys.VERSION, "1.0");
            if (!(root instanceof Document)) {
                trans.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "yes");
            }
            trans.transform(new DOMSource(root), new StreamResult(writer));

//            System.out.println(writer.toString());
            return writer.toString();

        }catch (Exception e){
            e.printStackTrace();
        }
        return null;
    }

    private static Document convertStringToXMLDocument(String xmlString)
    {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = null;
        try
        {
            builder = factory.newDocumentBuilder();
            Document doc = builder.parse(new InputSource(new StringReader(xmlString)));
            return doc;
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }
        return null;
    }

    public static void stringToGrammar(String grammar){
        try {
            Document document = convertStringToXMLDocument(grammar);

            DOMSource source = new DOMSource(document);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("grammar.xml").getPath());
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

    public static void stringToRule(String rule){

        try {
            Document document = convertStringToXMLDocument(rule);
            Element docRoot = document.getDocumentElement();

            File file = new File(Main.class.getClassLoader().getResource("grammar.xml").toURI());
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(file);
            Element root = doc.getDocumentElement();

            Element ruleEl = root;
            NodeList nodeList = doc.getElementsByTagName("rule");
            int j = 0;
            if (nodeList != null && nodeList.getLength() > 0) {
                for (j = 0; j < nodeList.getLength(); j++) {
                    Element el = (org.w3c.dom.Element) nodeList.item(j);
                    if (el.hasAttribute("id") && el.getAttribute("id").equals(docRoot.getAttribute("id"))) {
                        ruleEl = el;
                        break;
                    }
                }
            }
            root.removeChild((Node) ruleEl);
            Node firstDocImportedNode = doc.importNode(docRoot, true);
            root.insertBefore(firstDocImportedNode, nodeList.item(j));
//            root.appendChild(firstDocImportedNode);

            DOMSource source = new DOMSource(doc);

            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");
            transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
            StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("grammar.xml").getPath());
            transformer.transform(source, result);
        }catch (Exception e){
            e.printStackTrace();
        }

    }

}
