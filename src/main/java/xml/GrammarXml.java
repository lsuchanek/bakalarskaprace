package xml;

import backend.Main;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;

/**
 * Created by lsuchanek on 15.11.2018.
 */
public class GrammarXml {
    /*public static void main(String[] args) throws ParserConfigurationException, TransformerException, SAXException, IOException, URISyntaxException {
        addRule("city", "", null);
    }*/

    public static void addRule(String id, String value, String path) throws ParserConfigurationException, IOException, SAXException, TransformerException, URISyntaxException {
        File file = new File(Main.class.getClassLoader().getResource("grammar.xml").toURI());
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
        Document doc = dBuilder.parse(file);
        value = value.replace(": ", ":");
//        boolean find = false;
//        NodeList nodeList = doc.getElementsByTagName("rule");
//        if (nodeList != null && nodeList.getLength() > 0) {
//            for (int j = 0; j < nodeList.getLength(); j++) {
//                Element el = (org.w3c.dom.Element) nodeList.item(j);
//                if(el.hasAttribute("id") && el.getAttribute("id").equals(id)){
//                    find = true;
//                    if (value.toLowerCase().startsWith("example:")) {
//                        Element example = doc.createElement("example");
//                        example.appendChild(doc.createTextNode(value.substring(8)));
//                        el.appendChild(example);
//                    } else if(value.toLowerCase().startsWith("ref:")){
//                        NodeList nodes = el.getElementsByTagName("one-of");
//                        Element oneOf = (Element) nodes.item(0);
//                        Element newItem = doc.createElement("item");
//                        Element ref = doc.createElement("ruleref");
//                        ref.setAttribute("uri", "#" + value.substring(4));
//                        newItem.appendChild(ref);
//                        oneOf.appendChild(newItem);
//
//                    } else {
//                        NodeList nodes = el.getElementsByTagName("one-of");
//                        Element oneOf = (Element) nodes.item(0);
//                        Element newItem = doc.createElement("item");
//                        newItem.appendChild(doc.createTextNode(value));
//                        oneOf.appendChild(newItem);
//                    }
//                }
//            }
//        }

            Element root = doc.getDocumentElement();
            Element rule = doc.createElement("rule");
            rule.setAttribute("id", id);
            rule.appendChild(doc.createTextNode(" "));
            root.appendChild(rule);

        DOMSource source = new DOMSource(doc);

        TransformerFactory transformerFactory = TransformerFactory.newInstance();
        Transformer transformer = transformerFactory.newTransformer();
        transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
        transformer.setOutputProperty(OutputKeys.INDENT, "yes");
        transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
        StreamResult result = new StreamResult(Main.class.getClassLoader().getResource("grammar.xml").getPath()/*".\\src\\main\\resources\\grammar.xml"*/);
        transformer.transform(source, result);
    }
}
