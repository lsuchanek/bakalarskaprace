CREATE TABLE users (
    user_id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    age VARCHAR (255),
    sex VARCHAR (255)
);
CREATE TABLE conversation (
    conversation_id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    user_id BIGINT,
    grammar_id BIGINT,
    user_evaluation FLOAT,
    wizard_evaluation FLOAT,
    active_conversation CHAR(1),
    has_wizard CHAR(1)
);
CREATE TABLE turns (
    turn_id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    conversation_id BIGINT,
    question VARCHAR (255),
    answer VARCHAR (255),
    user_initialized CHAR (1)
);
CREATE TABLE grammar (
    grammar_id BIGINT NOT NULL PRIMARY KEY GENERATED ALWAYS AS IDENTITY,
    grammar_path VARCHAR (255),
    grammar_name VARCHAR (255)
);
