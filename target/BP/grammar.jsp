<%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 07.03.2019
  Time: 15:15
  To change this template use File | Settings | File Templates.
--%>
<%@ page import="java.io.*"%>
<%@ page import="backend.Main" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
</head>
<body>
<%
    try {
        File file = new File(Main.class.getClassLoader().getResource("grammar.html").toURI());
        FileReader fileReader = new FileReader(file);
        BufferedReader bufferedReader = new BufferedReader(fileReader);
        StringBuffer stringBuffer = new StringBuffer();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            out.println(line);
        }
        fileReader.close();
    } catch (IOException e) {
        e.printStackTrace();
    }
%>
</body>
</html>
