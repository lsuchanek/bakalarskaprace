<?xml version="1.0"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                version="1.0"
                xmlns:xsp="http://www.w3.org/1999/XSL/Transform">
    <xsl:template match="/">
        <html>
            <head>
            </head>
            <body>
                <h2>Rules</h2>
                <ul>
                   <xsl:apply-templates select="grammar/rule"/>
                </ul>
            </body>
        </html>
    </xsl:template>
    <xsl:template match="rule">
        <li>
            ID: <xsl:value-of select="@id"/><br/>
            <xsl:apply-templates/>
        </li>
    </xsl:template>

    <xsl:template match="ruleref">
        <xsl:choose>
            <xsl:when test="@special">$<xsl:value-of select="@special"/></xsl:when>
            <xsl:otherwise>
                <ul>
                    <xsl:variable name="uri" select="@uri"/>
                    <xsl:variable name="uriStr" select="substring($uri,1)"/>
                    <xsl:choose>
                        <xsl:when test="not(contains($uriStr, '#'))">
                            <li>
                                <xsl:text>Referenced grammar: </xsl:text>
                                <a href="{$uriStr}">
                                    <xsl:value-of select="$uriStr"/>
                                </a>
                                <xsl:text> root rule</xsl:text>
                            </li>
                        </xsl:when>
                    </xsl:choose>

                    <xsl:variable name="grammarUri" select="substring-before($uri, '#')"/>
                    <xsl:variable name="ruleId" select="substring-after($uri, '#')"/>
                    <xsl:choose>
                        <xsl:when test="string-length($grammarUri) = 0">

                            <xsl:apply-templates select="//rule[@id=$ruleId]"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <li>
                                <xsl:text>Referenced grammar: </xsl:text>
                                <a href="{$grammarUri}">
                                    <xsl:value-of select="$grammarUri"/>
                                </a>
                                <xsl:text> ruleID: </xsl:text>
                                <xsl:value-of select="$ruleId"/>
                            </li>
                        </xsl:otherwise>
                    </xsl:choose>
                </ul>
            </xsl:otherwise>
        </xsl:choose>

    </xsl:template>

    <xsl:template match="example">
        EXAMPLE: <xsl:value-of select="current()"/><br/>
    </xsl:template>

    <xsl:template match="one-of">
        (<xsl:for-each select="item">
            <xsl:apply-templates select="current()"/>/
        </xsl:for-each>)
    </xsl:template>

    <xsl:template match="item">
        <xsl:choose>
            <xsl:when test="count(item) + count(one-of) + count(ruleref) = 0">
                <xsl:apply-templates/>
            </xsl:when>
            <xsl:otherwise>
                {
                    <xsl:apply-templates/>
                }
            </xsl:otherwise>
        </xsl:choose>

        <xsl:if test="@repeat">
            (<xsl:value-of select="@repeat"/>)
        </xsl:if>
    </xsl:template>

    <xsl:template match="token">
        <xsl:value-of select="current()"/> <br/>
    </xsl:template>
    
</xsl:stylesheet>