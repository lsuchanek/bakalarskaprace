<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dialog Testing</title>
</head>
<body>
<center>

    <h1>
        Select role
    </h1>
    <h2>User</h2>
    <form method="post" action="User">
        <br> Age:
        <select name="Age" size="1">
            <option><20</option>
            <option>20-60</option>
            <option>>60</option>
        </select>
        <br><br>
            Sex:
        <select name="Sex" size="1">
            <option>MALE</option>
            <option>FEMALE</option>
        </select>
        <br><br>
        <input type="submit" value="Start Conversation">
    </form>
    <br>
    <h2>Log in as wizard</h2>
    <form method="post" action="WizardLogIn">
        Login: <input type="text" name="login"><br><br>
        Password: <input type = "password" name = "pass"><br><br>
        <%
            if(session.getAttribute("log") != null && session.getAttribute("log").equals(true)){
                out.println("<p style=\"color:red;\"><b>");
                out.println("Either login or password are incorrect! Please try again");
                out.println("</b></p>");
            }
        %>
        <input type="submit" value="Log In">
    </form>
    <br>
    <h2>Or create new wizard account</h2>
    <button onclick="myFunction()">Create new account</button>
    <div id="new" style="display:none;">
        <form method="post" action="WizardSignIn">
            Login: <input type="text" name="login"><br><br>
            Password: <input type = "password" name = "pass"><br><br>
            <%
                if(session.getAttribute("sign") != null && session.getAttribute("sign").equals(true)){
                    out.println("<p style=\"color:red;\"><b>");
                    out.println("Entered login is already used! Please choose different login");
                    out.println("</b></p>");
                }
            %>
            <input type="submit" value="Sign In">
        </form>
    </div>
</center>

<script>
    function myFunction() {
        var e = document.getElementById("new");
        e.style.display = (e.style.display == 'block') ? 'none' : 'block';
    }
</script>
</body>
</html>