<%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 08.05.2019
  Time: 10:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>
        Starter page
    </title>
</head>
<body>
<h2>${login}</h2>
<form action="./">
    <input type="submit" value="Log out" />
</form>
<a href="changePassword.jsp" >Change password</a>
<form method="post" action="JoinConversation">
    <h3>Join conversation</h3>
    <%
        out.println("<input type=\"hidden\" name=\"login\" value=\""+ session.getAttribute("login") +"\">");
        if(request.getAttribute("noConversation") != null){
            out.println("<p style=\"color:red;\">");
            out.println("No conversation available! Please try later");
            out.println("</p>");
        }
    %>
    <input type="submit" value="Join Conversation">
</form>

<h2>Tested grammar</h2>
<iframe src="grammar.jsp" width="500" height="500">
</iframe>
<form method="get" action="RuleNoCon">
    ID: <input type="text" name="id">
    <input type="submit" value="Choose rule">
</form>

<form method="post" action="ChangeRule">

         <textarea rows = "10" cols = "50" name="changedRule">
             <%
                 out.println(session.getAttribute("rule"));
             %>
         </textarea>
    <%
        out.println("<input type=\"hidden\" name=\"id\" value=\""+ session.getAttribute("ruleID") +"\">");
    %>
    <input type="submit" value="Change rule">

</form>

</body>
</html>