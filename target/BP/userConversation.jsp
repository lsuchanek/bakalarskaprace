<%@ page import="backend.User" %>
<%@ page import="java.util.List" %>
<%@ page import="backend.Turn" %>
<%@ page import="backend.Conversation" %>
<%@ page import="xml.ConversationManager" %><%--
  Created by IntelliJ IDEA.
  User: lsuchanek
  Date: 11.09.2018
  Time: 13:53
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Chat</title>
    <style>
        div {
            width: 500px;
            padding: 10px;
            border: 2px solid black;
            margin-right: auto;
            margin-left: auto;
        }
    </style>
    <script type="text/javascript" src="jquery-3.3.1.js"></script>
    <script type="text/javascript">
        var auto_refresh = setInterval(
            function ()
            {
                $('#load_me').load('showdata.jsp').fadeIn("slow");
            }, 1000); // autorefresh the content of the div after
                       //every 10000 milliseconds(10sec)
    </script>
</head>
<body>
<center>
<h1>
    Chat
</h1>
    <div id="load_me">
        <%@ include file="showdata.jsp" %>
    </div>
<br>
<form method="post" action="UserReply">
    <input type="text" name="answer" size="45">
    <%
        out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
    %>
    <input type="submit" value="Send"><br><br>

</form>
<form method="post" action="UserEnd">
    Evaluation [0.0-10.0]: <input type="text" name="evaluation">
    <%
        out.println("<input type=\"hidden\" name=\"conId\" value=\""+ conversation.getId().toString() +"\">");
    %>
    <input type="submit" value="End conversation"><br><br>

</form>
</center>
</body>
</html>
